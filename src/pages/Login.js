import	React, { useState, useEffect } from 'react';
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState ('');
	const [password, setPassword]= useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';

		if ( isEmailNotEmpty && isPasswordNotEmpty ) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[ email, password ]);

	function login(e){
		e.preventDefault();

		Swal.fire('You are now logged in.')

		// clears the input fields since they update their respective variable values into an empty string
		setEmail('');
		setPassword('');
	}

	return(
			<Container fluid>
				<Form onSubmit={login}>
					<h1>Login</h1>
					<Form.Group>
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
					</Form.Group>

					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>

					<Button variant="primary" type="submit" disabled={isDisabled}>Login</Button>
				</Form>
			</Container>
		)
}

